{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./keeds.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages_3_14;
  # boot.kernelPackages = pkgs.linuxPackages_3_18;
  # boot.kernelModules = [ "msr" ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  # swapDevices =[ { label = "Swap"; } ];
  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];
  
  networking.hostName = "pixie"; # Define your hostname.
  networking.wireless.enable = true;  # Enables wireless.

  # Select internationalisation properties.
  i18n = {
    consoleFont = "lat9w-16";
    consoleKeyMap = "uk";
    defaultLocale = "en_GB.UTF-8";
  };

  time.timeZone = "Europe/London";

  services.openssh.enable = true;
  services.virtualboxHost.enable = true;
  # services.docker.enable = true;
  
  users.extraUsers.guest = {
    name = "keeds";
    group = "users";
    extraGroups = [ "wheel" "disk" "audio" "video" "networkmanager" "systemd-journal" ];
    uid = 1000;
    createHome = true;
    home = "/home/keeds";
    shell = "/run/current-system/sw/bin/bash";
  };

  services.xserver.wacom.enable = true;
  services.xserver.synaptics = {
    enable = true;
    buttonsMap = [ 1 2 3 ];
    palmDetect = true;
    tapButtons = true;
    twoFingerScroll = true;
  };

}
