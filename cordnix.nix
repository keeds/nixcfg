# nix-env -f cordnix.nix -i env-cordnix -> load-env-cordnix
# ./load-env-cordnix

with import <nixpkgs> {};

myEnvFun {
  name = "cordnix";
  buildInputs = [
    nodejs
    nodePackages.cordova
    androidenv.androidsdk_4_4
    jdk
    ant
    which
    gnumake
  ];
  extraCmds = ''
    export ANDROID_HOME=${androidenv.androidsdk_4_4}/libexec/android-sdk-linux
    export GNUMAKE=${gnumake}/bin/make
  '';
}
