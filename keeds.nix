{ config, pkgs, ... }:

{

  nixpkgs.config = {
    allowUnfree = true;

    firefox = {
      enableAdobeFlash = true;
    };

    chromium = {
      enablePepperFlash = true;
      enablePepperPDF = true;
    };

  };

  environment.systemPackages = with pkgs; [

    # essentials
    tmux htop wget cowsay

    # build essentials
    binutils gcc gnumake pkgconfig python ruby

    # desktop essentials
    dmenu

    # desktop apps
    chromium firefoxWrapper
  ];

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = [
       pkgs.terminus_font
       pkgs.anonymousPro
       pkgs.corefonts
       pkgs.freefont_ttf
       pkgs.source-code-pro
       pkgs.ttf_bitstream_vera
    ];
  };

  services = {
    nixosManual.showManual = true;
  };
  
  services.xserver = {
    enable = true;
    windowManager.i3.enable = true;
    displayManager.lightdm.enable = true;
    desktopManager.xterm.enable = false;
    layout = "gb";
    xkbOptions = "ctrl:nocaps,eurosign:e";
  };

  # services.acpid = {
  #   enable = true;
  #   lidEventCommands = ''
  #     LID="/proc/acpi/button/lid/LID/state"
  #     state=`cat $LID | ${pkgs.gawk}/bin/awk '{print $2}'`
  #       case "$state" in
  #         *open*) echo $(whoami) > /home/keeds/whoami ;;
  #         *close*) pm-suspend ;;
  #         *) logger -t lid-handler "Failed to detect lid state ($state)" ;;
  #       esac
  #     '';
  # };

  powerManagement.enable = true;

  containers.pgdb =
  { config =
      { config, pkgs, ... }:
      { services.postgresql.enable = true;
        services.postgresql.package = pkgs.postgresql93;
      };
  };

}
